###############################################################################
#                              Projects Settings                              #
###############################################################################

cmake_minimum_required(VERSION 3.2)
project(HiCViewer3D_cpp)

###############################################################################
#                              Compilation Flags                              #
###############################################################################
list(APPEND CMAKE_CXX_FLAGS "-std=c++11")
add_definitions(-Wall)

###############################################################################
#                                Load Libraries                               #
###############################################################################
# glfw
set(GLFW_BUILD_EXAMPLES OFF CACHE STRING "" FORCE)
set(GLFW_BUILD_TESTS    OFF CACHE STRING "" FORCE)
set(GLFW_INSTALL        OFF CACHE STRING "" FORCE)
add_subdirectory(extern/glfw)
include_directories(extern/glfw/include)

# glew
add_definitions(-DGLEW_STATIC)
option(glew-cmake_BUILD_SHARED "" OFF)
add_subdirectory(extern/glew)
include_directories(extern/glew/include extern/glew/src)

# glm
include_directories(extern/glm)

###############################################################################
#                                Build on Tests                               #
###############################################################################

option(PACKAGE_TESTS "Build the tests" ON)
if(PACKAGE_TESTS)
    enable_testing()
    add_subdirectory(test)
endif()
add_subdirectory("${PROJECT_SOURCE_DIR}/extern/googletest" "extern/googletest")
mark_as_advanced(
    BUILD_GMOCK BUILD_GTEST BUILD_SHARED_LIBS
    gmock_build_tests gtest_build_samples gtest_build_tests
    gtest_disable_pthreads gtest_force_shared_crt gtest_hide_internal_symbols
)
set_target_properties(gtest PROPERTIES FOLDER extern)
set_target_properties(gtest_main PROPERTIES FOLDER extern)
set_target_properties(gmock PROPERTIES FOLDER extern)
set_target_properties(gmock_main PROPERTIES FOLDER extern)
macro(package_add_test TESTNAME)
  include_directories(
    ${OPENGL_INCLUDE_DIRS}
    ${GLFW_INCLUDE_DIRS}
    ${GLEW_INCLUDE_DIRS}
    ${GLM_INCLUDE_DIRS}
    )
  add_executable(${TESTNAME} ${ARGN}  )
  target_link_libraries(${TESTNAME} gtest gmock gtest_main
    ${OPENGL_LIBRARY} # filled by "find_package(OpenGL REQUIRED)"
    ${GLFW_LINK_LIBRARIES}
    ${GLEW_LINK_LIBRARIES}
    ${OPENGL_LIBRARIES})
  add_test(${TESTNAME} COMMAND ${TESTNAME} WORKING_DIRECTORY ${UNIT_TEST_BIN_OUTPUT_DIR})
  set_target_properties(${TESTNAME} PROPERTIES FOLDER tests)
endmacro()

package_add_test(unit-tests ../test/unit_tests.cpp)
###############################################################################
#                               Build Executable                              #
###############################################################################
file(GLOB_RECURSE source_files ${PROJECT_SOURCE_DIR}/src/main.cpp)
add_executable(main ${source_files})
target_link_libraries(main glfw ${GLFW_LIBRARIES} libglew_static)

###############################################################################
#                             System Notifications
#
###############################################################################

# HOW-TO. You can safely remove anything below.  

# test if linux, mac or windows : 

if(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
	message("You're on Mac OS !")
elseif(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
	message("You're on Linux !")
elseif(${CMAKE_SYSTEM_NAME} MATCHES "Windows")
	message("You're on Windows !")
endif(${CMAKE_SYSTEM_NAME} MATCHES "Darwin") # Yes this is weird but you have to repeat the "if" here.


# Add a checkbox in CMakeGUI : 

set(SOME_STUFF OFF CACHE BOOL "Check this to print an extra message at the beginning of main()")

# Add a preprocessor define : 

if(SOME_STUFF)
	add_definitions(
		-DCOMPILE_WITH_SOME_PREPROCESSOR_DIRECTIVE
	)
endif(SOME_STUFF)
