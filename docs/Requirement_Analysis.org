#+OPTIONS: reveal_center:t reveal_progress:t reveal_history:t reveal_control:t
#+OPTIONS: reveal_mathjax:t reveal_rolling_links:t reveal_keyboard:t reveal_overview:t num:nil
#+OPTIONS: reveal_width:1200 reveal_height:800
#+REVEAL_ROOT: file:///Users/k/Apps/reveal.js
#+REVEAL_HLEVEL: 2
#+REVEAL_MARGIN: 0.2
#+REVEAL_MIN_SCALE: 0.5
#+REVEAL_MAX_SCALE: 1.5
#+REVEAL_EXTRA_CSS: ./hic3dviewpre.css


* HiC 3D Viewer 需求分析文档

** 需求分析

#+CAPTION: Stages of API development from analysis to design to implementation.png
[[file:~/org-wiki/C++ API design/Stages of API development from analysis to design to implementation.png]]

*** General Description
HiCviewer will be a cross-platform, portable HiC sequence 3D viewer. It is can be easily implemented into a existing functions and act as plugins.
Here are some general targets of our software:
- Cross-platform and Web friendly
  - Runnable on cross-platform OS,i.e Windows, OSX.
  - Offer Docker image for fast deploy
  - With a server version than could provide web services
- Plugins-like
  - Code highly reusable that could be reused for different projects
- Expansible 
  - Offer a standard API lists which could be further used for other algorithms 

*** 总体分析

- 舒适的3D显示
  - Shader渲染，更加有质感的渲染
  - 景深控制
    - 过于靠近的前景要透明
    - 过远的背景要去除
- 接口控制
  - 可以使用默认的设置来显示
  - 可以使用个性化的代码来修改算法，数据等等
  - 成型的软件库接口，可以方便调用
- 2D图形的显示
  - 可以轻松调用R,python等脚本语言进行操作
  - 内置小控制台调用R和Python交互式脚本
- 事件引擎
  - 定量地搜索指定条件地定制事件，如相互作用等等。。。 
*** Requirements
| Software       | Platform            | Requirements |
|----------------+---------------------+--------------|
| PC version     | Linux, OSX, Windows | OpenGL       |
| Server version | Linux server        | WebGL        |

*** PC Version
- OS requirements:
  - Linux distributions, OSX, and Windows
- Environment requirements:
  - OpenGL supportable
  - Docker(optional)
  - CMake
- Libs
  - OpenGL
  - GLUT
  - OSG/OGRE(?)
  - Ribbon[fn:2]
 
*** User stories
#+HTML: <div style="font-size:70%;">
Format
#+BEGIN_QUOTE
As a [role] I want [something] so that [benefit].
#+END_QUOTE
#+HTML: </div>
#+HTML: <div style="font-size:50%;">
- 作为一个研究人员，我希望可以使用HiC Viewer的3D功能来更直观地观察生成的3维模型。
- 作为一个研究人员，我希望可以使用HiC Viewer的显示相互作用功能来显示染色体间的相互作用。
- 作为一个研究人员，我希望可以使用HiC Viewer的显示相互作用功能来显示染色体内的相互作用。
- 作为一个研究人员，我希望可以使用HiC Viewer的放缩功能来能够放大和缩小显示的区域。
- 作为一个研究人员，我希望可以使用HiC Viewer的渲染和纹理机制功能来舒适地陷。
- 作为一个研究人员，我希望可以使用HiC Viewer的染色体功能来轻松地选择显示的染色体。
- 作为一个研究人员，我希望可以使用HiC Viewer的数据读取功能来读取多种格式的HiC输入数据。
- 作为一个研究人员，我希望可以使用HiC Viewer的导出功能来导出HiC矩阵数据。
- 作为一个研究人员，我希望可以使用HiC Viewer的导出功能来导出指定样式的图片。
- 作为一个研究人员，我希望可以使用HiC Viewer的颜色分配功能来自动指定染色体颜色。
- 作为一个研究人员，我希望可以使用HiC Viewer的过滤功能来筛选符合条件的染色体或loci间作用。
- 作为一个研究人员，我希望可以使用HiC Viewer的注释功能来查看注释。
- 作为一个研究人员，我希望可以使用HiC Viewer的注释修改功能来修改注释。
- 作为一个研究人员，我希望可以使用HiC Viewer的鼠标功能来放缩和聚焦到指定loci上。
- 作为一个研究人员，我希望可以使用HiC Viewer的loci选取来选择到指定loci上。

- 作为一个研究软件开发者，我希望可以使用HiC Viewer的库功能来为我自己的软件添加类似功能。
- 作为一个研究软件开发者，我希望可以使用HiC Viewer的插件功能来读取定制化的数据类型。
- 作为一个研究软件开发者，我希望可以使用HiC Viewer的插件功能来实现定制化的算法。
- 作为一个研究软件开发者，我希望可以使用HiC Viewer的安装功能来跨平台轻松安装。
#+HTML: </div>

*** 模块设计
#+HTML: <div style="width:80%;height:80%;margin:auto">
#+CAPTION: HiC模块设计图
[[file:~/org-wiki/Graduate Projects/HiC模块设计图.png]]
#+HTML: </div>

*** 其他功能
    - 载入GAM-Matrix可视化
    - diff功能，比较相互作用的差异


** 数据处理

*** 输入数据

*** HiC Matrix 数据
    #+BEGIN_SRC
    |   |        1 |       2 |       3 |       4 |
    |---+----------+---------+---------+---------|
    | 1 | 681.6289 | 19.4844 |  0.0000 |  0.0000 |
    | 2 |  19.4844 | 80.2030 |  0.8227 |  0.0000 |
    | 3 |   0.0000 |  0.8227 | 59.5417 |  8.0446 |
    | 4 |   0.0000 |  0.0000 |  8.0446 | 72.1848 |
    #+END_SRC

    - HiC Matrix Input Format
    #+BEGIN_SRC
         | (Loci_A) | (Loci_B) | (distance) |
         |----------+----------+------------|
         |       55 |    16026 |   3.062183 |
         |       55 |    16048 |   1.230560 |
         |       55 |    16051 |   1.346437 |
         |       55 |    16111 |   2.115605 |
         |       56 |    15920 |   3.938922 |
         |       56 |    16030 |   1.304563 |
         |       56 |    16032 |   0.135079 |
         |       56 |    16033 |   0.171104 |
         |       56 |    16040 |   0.174144 |
         |       56 |    16043 |   0.297062 |
         |       56 |    16045 |   0.134199 |

    #+END_SRC

*** 3D模型数据

**** Plain text

    #+BEGIN_SRC
     | chrom | locus |  X3D_x |   X3D_y |   X3D_z |
     |-------+-------+--------+---------+---------|
     |     1 |     1 | 96.509 |  99.910 | 114.877 |
     |     1 | 18848 | 96.251 | 100.727 | 115.392 |
     |     1 | 37695 | 95.992 | 101.545 | 115.907 |
     |     1 | 56542 | 95.733 | 102.362 | 116.421 |
     |     1 | 75390 | 95.474 | 103.179 | 116.936 |
     |     1 | 94237 | 95.216 | 103.996 | 117.451 |
    #+END_SRC

**** PDB 数据[fn:1]

*** 注释数据
    #+BEGIN_SRC
    | 1 |  760000 | 1280000 | 2.47032822920623e-322 |
    | 1 | 1280000 | 1840000 |                     0 |
    | 1 | 1840000 | 2320000 |                     0 |
    | 1 | 2320000 | 3600000 |              6.495675 |
    | 1 | 3760000 | 6000000 |      2.41722222222222 |
    | 1 | 6200000 | 6440000 |                     0 |
    #+END_SRC

** 3D模型预测算法

*** Metric-MDS
    Given Hi-C Frequency matrix $F$, the algorithm calculates for each pair of bins $(i, j)$ an estimated distance $\delta_{ij}=(\frac{1}{f_{ij}})^\alpha$. And find the best spatial 3D positioning that by optimizing the following equation.
$$
\min\limits_{x}\sum\limits_{(i,j)\in D}(||x_i - x_j|| - \delta_{ij})^2
$$

*** Non-metric MDS(NMDS)
if two points i and j have a higher Hi-C contact frequency than another two, k,l, the $i-j$ should be closer than $k-l$.
$$
c_{ij}\ge c_{kl} \Leftrightarrow ||x_i-x_j||_2\le||x_k-x_l||_2
$$

*** Possion Model 1 (PM1)
contact frequency $c_{ij}$ is modeled as an independent Possion random variable with $\lambda=\beta d_{ij}(X)^\alpha$ such as $d_{ij}(X)$.
The likelihood of having a certain 3D positioning $X$ can be calculated as:
$$
l(X,\alpha, \beta)=\prod\limits_{i,j}\frac{(\beta d_{ij}^\alpha)^{c_{ij}}}{c_{ij}!}e^{-\beta d_{ij}^\alpha}
$$
The PM1 algorithm tries to find the best $\beta$ that maximizes the log-likelihood given $\alpha$ fixed.
$$
\max\limits_{\alpha, \beta, X} L(X,\alpha,\beta)=\sum\limits_{i\le j\le n}c_{ij}\alpha log(d_{ij}) + c_{ij}log(\beta) + \beta d_{ij}^\alpha
$$

*** PM2
PM2 use a non-parametric approach to estimate $\alpha$ and $\beta$ in PM1 model.
** 渲染引擎
*** OpenGL
    #+BEGIN_SRC dot :file pre-2019-01-01.png :exports results
    Digraph G {
        subgraph cluster_0 {
	sytle=filled;
	color=lightgrey;
	node [style=filled, color=white];
	"坐标生成" -> "载入图元模型" -> "Vertex & Fragment Shaders" -> "Lighting" -> "Camera translation" -> "Projection";
	label="3维展示";
	}
	"原始输入" -> "坐标生成";
    }

    #+END_SRC

    #+RESULTS:
    [[file:pre-2019-01-01.png]]

*** GLFW GLEW
** 时间计划

#+HTML: <div style="font-size:60%">
|---------------------------------+-----------+------------------|
| Functions                       | Version   | Timing           |
|---------------------------------+-----------+------------------|
|---------------------------------+-----------+------------------|
| Default Data Process            | Version 1 | 1.5 Month        |
| Coordinate mapping              | Version 1 | 1.5 Month        |
| Graphic primitive               | Version 1 | 1.5 Month        |
| Shader                          | Version 1 | 1.5 Month        |
| Camera                          | Version 1 | 1.5 Month        |
| Event engine & Filter Functions | Version 1 | 1.5 Month        |
| Local Default Engine            | Version 1 | 1.5 Month        |
| Annotation Manager              | Version 1 | 1.5 Month        |
| Packaged & Release              |           | <2019-02-12 Tue> |
|---------------------------------+-----------+------------------|
| 3D Editor                       | Version 2 | 1 Month          |
| 2D Plot Algorithm               | Version 2 | 1 Month          |
| Plugin Manager                  | Version 2 | 1 Month          |
| 2D Plot Engine                  | Version 2 | 1 Month          |
| Packaged & Release              |           | <2019-03-14 Thu> |
| Community Builds                |           | <2019-03-14 Thu> |
|---------------------------------+-----------+------------------|
| Commandline Engine              | Version 3 | 1 Month          |
| Other scripts Wrapper           | Version 3 | 1 Month          |
| Packaged & Release              |           | <2019-04-13 Sat> |
#+HTML: </div>

* Notes

** Mapping 1D-2D-3D

** New Algorithm

** Web3DMol
Web3DMol: Interactive protein structure visualization based on WebGL

* Versions
| Version |      Dates | Comments                                                    |
|---------+------------+-------------------------------------------------------------|
|     1.0 | 2019-01-02 | Workshop presentation of requirement analysis on 2019-01-02 |
|         |            |                                                             |

* Footnotes

[fn:2]: http://www.msg.ucsf.edu/local/programs/ribbons/ 

[fn:1]: http://www.wwpdb.org/documentation/file-format-content/format33/v3.3.html
