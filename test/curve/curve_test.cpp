#include "../../src/curve/curve.cpp"

#include <gtest/gtest.h>



TEST( CurveTest, TestAddPoints )
{
  Curve a ;
  glm::dvec3 point_1(0.0, 1.0, 2.0);
  glm::dvec3 point_2(0.1, 1.1, 2.1);
  a.Curve::add_curve_points(point_1);
  a.Curve::add_curve_points(point_2);
  a.Curve::info();
  EXPECT_EQ((*a.Curve::get_curve_points())[0],point_1);

}


TEST( CurveReset, TestResetCurve )
{
  Curve a ;
  glm::dvec3 point_1(0.0, 1.0, 2.0);
  glm::dvec3 point_2(0.1, 1.1, 2.1);
  a.Curve::add_curve_points(point_1);
  a.Curve::add_curve_points(point_2);
  EXPECT_EQ(*(a.Curve::get_curve_size()), 2);
  a.Curve::reset_curve_data();
  EXPECT_EQ(*(a.Curve::get_curve_size()), 0);
}
