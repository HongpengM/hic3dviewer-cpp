#include <gtest/gtest.h>
#include "gtest-example.cpp"
#include "curve/curve_test.cpp"

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
