#include "curve.hpp"
Curve::Curve(){
  //  this->_points = new vector<glm::dvec3>;
  //  this->_distances = new vector<double>;
  this->_size = 0;
}
Curve::~Curve(){
  
}
void Curve::reset_curve_data(){
  this->_points.clear();
  this->_distances.clear();
  this->_size = 0;
 
}
int Curve::add_curve_points(glm::dvec3 & new_point){
  _points.push_back(new_point);
  ++_size;
  if (_size==1)
    {
      _distances.push_back(0);
    }
  else
    {
      _distances.push_back(static_cast<double>(glm::distance(_points[_size - 1], _points[_size - 2])));
    }
  return _size;
}

int* Curve::get_curve_size(){
  return &(this->_size);
}
std::vector<glm::dvec3>* Curve::get_curve_points(){
  std::vector<glm::dvec3>* curve_points = &(this->_points);
  return curve_points;
}
#ifdef __DEBUG__
void Curve::info(){
  std::cout<<"Curve info:\n==============================\n"<<std::endl;
  for (int i = 0; i < _size; i++) {
    std::cout<<"Coordinate: ["<<std::to_string(_points[i][0])<<","<<std::to_string(_points[i][1])<<","<<std::to_string(_points[i][2])<<"], Distance: "<<std::to_string(_distances[i])<<std::endl;
  }
  std::cout<<"========== Ouput Finished==========\n";
}


#endif // __DEBUG__
