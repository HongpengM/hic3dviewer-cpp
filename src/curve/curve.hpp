#ifndef CURVE_H
#define CURVE_H
#include <vector>
#define __DEBUG__
#include <glm/glm.hpp>
#include <iostream>

#endif /* CURVE_H */

class Curve{
public:
  Curve();
  ~Curve();
  // Reset the all private data in curves
  void reset_curve_data();
  // Add a new point to current curves
  int add_curve_points(glm::dvec3 & new_point);

  int* get_curve_size();
  std::vector<glm::dvec3>* get_curve_points();
  #ifdef __DEBUG__
    // Print the inside data in curves
  void info();
  #endif // __DEBUG__

  
private:
  std::vector<glm::dvec3> _points;
  std::vector<double> _distances;
  int _size;
};
