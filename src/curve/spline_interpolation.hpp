#ifndef SPLINE_INTERPOLATION_H
#define SPLINE_INTERPOLATION_H

#include "curve.hpp"


#endif /* SPLINE_INTERPOLATION_H */

class SplineInterpolation{
public:
  SplineInterpolation();
  ~SplineInterpolation();

private:
  Curve mCurve;
}
