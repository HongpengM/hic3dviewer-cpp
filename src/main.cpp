#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include "shaders/shader.cpp"


GLFWwindow * window;

void cardinal_spline(const GLdouble ** vertex){
  
};

int main(void)
{
  if (!glfwInit()){
    std::cerr<<"Failed to initialize GLFW\n";
    return -1;
  }

  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  
  window = glfwCreateWindow(1024, 768, "Tutorial 2", NULL, NULL);
  if (window == NULL){
    std::cerr <<"Failed to open GLFW window\n";
    glfwTerminate();
    return -1;
  }
  glfwMakeContextCurrent(window);
  glewExperimental=true;
  if (glewInit() != GLEW_OK){
    std::cerr<<"Failed to Initialize GLEW\n";
  }
  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

  //glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

  GLuint VertexArrayID;
  glGenVertexArrays(1, &VertexArrayID);
  glBindVertexArray(VertexArrayID);

  GLuint programID = LoadShaders("../src/shaders/SimpleVertexShader.vertexshader", "../src/shaders/SimpleFragmentShader.fragmentshader");

  static const GLfloat g_vertex_buffer_data[]={
					       -1.0f,-1.0f,0.0f,
					       1.0f,-1.0f,0.0f,
					       0.0f,1.0f,0.0f,
  };

  GLuint vertexbuffer;
  glGenBuffers(1, &vertexbuffer);
  glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);
  

  
  do {
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(programID);
    
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(
			  0,
			  3,
			  GL_FLOAT,
			  GL_FALSE,
			  0,
			  (void *)0
			  );
    
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glDisableVertexAttribArray(0);
    
    glfwSwapBuffers(window);
    glfwPollEvents();
  }
  while(glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0);

  glDeleteBuffers(1, &vertexbuffer);
  glDeleteVertexArrays(1, &VertexArrayID);
  glDeleteProgram(programID);
  
  glfwTerminate();
  return 0;
}

