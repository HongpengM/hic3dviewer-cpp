#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>

#include <stdlib.h>
#include <string.h>


#include <GL/glew.h>
#include "shader.hpp"

GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path)
{
  GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
  GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

  std::string VertexShaderCode;
  std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);

  if (VertexShaderStream.is_open())
    {
      std::string Line = "";
      while (getline(VertexShaderStream, Line))
	{
	  VertexShaderCode += "\n" + Line;
	}
      VertexShaderStream.close();
      
    }
  else
    {
      std::cout<<"Failed to open " <<vertex_file_path<<" Are you in the right directory? "<<std::endl;
      getchar();
      return 0;
    }

  std::string FragmentShaderCode;
  std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
  if (FragmentShaderStream.is_open())
    {
      std::string Line ="";
      while (getline(FragmentShaderStream, Line))
	{
	  FragmentShaderCode += "\n" + Line;
	}
      FragmentShaderStream.close();
    }
  else
    {
      std::cout<<"Failed to open "<< fragment_file_path << " Are you in the right directory? "<< std::endl;
      getchar();
      return 0;
    }

  GLint Result = GL_FALSE;
  int InfoLogLength;


  // Compile
  std::cout << "Compiling shader: " << vertex_file_path << std::endl;
  char const * VertexSourcePointer = VertexShaderCode.c_str();
  glShaderSource(VertexShaderID, 1, & VertexSourcePointer, NULL);
  glCompileShader(VertexShaderID);

  glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
  glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
  if ( InfoLogLength > 0)
    {
      std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
      glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
      std::cout << &VertexShaderErrorMessage[0] << std::endl;
    }
						      
  Result = GL_FALSE;
  
  std::cout << "Compiling shader: " << fragment_file_path << std::endl;
  char const * FragmentSourcePointer = FragmentShaderCode.c_str();
  glShaderSource(FragmentShaderID, 1, & FragmentSourcePointer, NULL);
  glCompileShader(FragmentShaderID);

  glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
  glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
  if ( InfoLogLength > 0)
    {
      std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
      glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
      std::cout << &FragmentShaderErrorMessage[0] << std::endl;
    }

  // Link

  std::cout<<"Linking program"<<std::endl;
  GLuint ProgramID = glCreateProgram();
  glAttachShader(ProgramID, VertexShaderID);
  glAttachShader(ProgramID, FragmentShaderID);
  glLinkProgram(ProgramID);

  // Check
  glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
  glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
  if (InfoLogLength > 0 )
    {
      std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
      glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
      std::cout<<&ProgramErrorMessage[0]<<std::endl;
    }

  glDeleteShader(VertexShaderID);
  glDeleteShader(FragmentShaderID);

  return ProgramID;
}
