#!/bin/bash
if [[ $1 == "-y" ]]; then
    character="y"
else
    echo -n "Are you sure to regenerate code coverage? Enter[y/n]"
    read character
fi;

if [[ $character == "y" ]]; then
    rm -rf build/* coverage_report
    rm coverage.in*
elif [[ $character == "n" ]]; then
    :
else
    exit -1;
fi;

mkdir -p build && cd build && g++ --coverage ../../test/unit_tests.cpp  -lgtest -I ../../extern/glm/ -I ../../extern/gl\
ew/include -I ../../extern/glfw/include -I /usr/local/include/ -pthread -std=c++11 \
    && ./a.out \
    && gcov unit_tests 	  
COVERAGE_FILE=coverage.info \
    && REPORT_FOLDER=coverage_report
cd ..
echo $PWD
lcov --rc lcov_branch_coverage=1 -c -d build -t unit_tests -o ${COVERAGE_FILE}_tmp  1>/dev/null 2>&1\
    && lcov --rc lcov_branch_coverage=1  -e ${COVERAGE_FILE}_tmp "*src*" -o ${COVERAGE_FILE} \
    && genhtml --rc genhtml_branch_coverage=1 ${COVERAGE_FILE} -o ${REPORT_FOLDER}

